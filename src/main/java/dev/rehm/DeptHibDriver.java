package dev.rehm;

import dev.rehm.data.DepartmentDao;
import dev.rehm.data.DepartmentDaoImpl;
import dev.rehm.data.MarketItemDao;
import dev.rehm.data.MarketItemDaoHibImpl;
import dev.rehm.models.Department;
import dev.rehm.models.MarketItem;

import java.sql.Date;

public class DeptHibDriver {

    public static void main(String[] args) {
        DepartmentDao dDao = new DepartmentDaoImpl();
        Department dept = new Department(5, "Camping", "VA");
//        dDao.createDepartment(dept);
        Department dept2 = new Department("Water Sports", "FL");
        Department dept3 = new Department("Hiking", "CO");
//        dDao.createDepartment(dept2);
//        dDao.createDepartment(dept3);

        Department dept4 = new Department("Climbing", "CA", new Date(1616772839298L));
        System.out.println(dept4);
        dDao.createDepartment(dept4);

        MarketItemDao miDao = new MarketItemDaoHibImpl();
        MarketItem item = new MarketItem(1500, "kayak", new Department(2));
        MarketItem item2 = new MarketItem(75, "paddle", new Department(2));
        MarketItem item3 = new MarketItem(10, "fire starter", new Department(1));
//        miDao.addNewItem(item2);
//        miDao.addNewItem(item3);
//        MarketItem item14 = miDao.getItemById(14);
//        System.out.println(item14.getDepartment().getId()); // we can access the ID but no other fields if it was never initialized beyond a proxy
//        System.out.println(item14.getDepartment().getName());




    }
}
