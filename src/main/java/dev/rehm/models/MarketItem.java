package dev.rehm.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

//@NamedQueries(@NamedQuery(name = "myQuery", query = "from ..."))

@Entity
@Table(name="market_item")
@JsonIgnoreProperties("hibernateLazyInitializer")
public class MarketItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private double price;

    private String name;

//    @ManyToOne(fetch = FetchType.LAZY)
    @ManyToOne
    private Department department;

    public MarketItem(){
        super();
    }

    public MarketItem(double price, String name) {
        this.price = price;
        this.name = name;
    }

    public MarketItem(int id, double price, String name) {
        this.id = id;
        this.price = price;
        this.name = name;
    }

    public MarketItem(double price, String name, Department dept) {
        this.price = price;
        this.name = name;
        this.department = dept;
    }

    public MarketItem(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MarketItem that = (MarketItem) o;
        return id == that.id && Double.compare(that.price, price) == 0 && Objects.equals(name, that.name) && Objects.equals(department, that.department);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price, name, department);
    }

    @Override
    public String toString() {
        return "MarketItem{" +
                "id=" + id +
                ", price=" + price +
                ", name='" + name + '\'' +
                ", department=" + department +
                '}';
    }
}
