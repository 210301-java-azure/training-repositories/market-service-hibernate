package dev.rehm.data;

import dev.rehm.models.Department;
import dev.rehm.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class DepartmentDaoImpl implements DepartmentDao {

    @Override
    public List<Department> getAllDepartments() {
        return null;
    }

    @Override
    public Department createDepartment(Department newDepartment) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            int id = (int) s.save(newDepartment);
            newDepartment.setId(id);
            tx.commit();
            return newDepartment;
        }
    }
}
