package dev.rehm.services;

import dev.rehm.data.MarketItemDao;
import dev.rehm.data.MarketItemDaoHibImpl;
import dev.rehm.models.MarketItem;
import io.javalin.http.BadRequestResponse;


import java.util.List;

public class MarketItemService {

    private final MarketItemDao marketItemDao = new MarketItemDaoHibImpl();

    public List<MarketItem> getAll(){
        return marketItemDao.getAllItems();
    }

    public MarketItem getById(int id){
        return marketItemDao.getItemById(id);
    }

    public MarketItem add(MarketItem item){
        return marketItemDao.addNewItem(item);
    }

    public void delete(int id){
        marketItemDao.deleteItem(id);
    }

    public List<MarketItem> getItemsInRange(String minPrice, String maxPrice) {
        if (minPrice != null) {
            if (matchesPriceFormat(minPrice)) {
                if (maxPrice != null) {
                    if (matchesPriceFormat(maxPrice)) {
                        return marketItemDao.getItemsInRange(Double.parseDouble(minPrice), Double.parseDouble(maxPrice));
                    }
                    throw new BadRequestResponse("improper price format provided for max-price");
                }
                // just min price
                return marketItemDao.getItemsWithMinPrice(Double.parseDouble(minPrice));
            }
            throw new BadRequestResponse("improper price format provided for min-price");
        } else {
            // just max price
                if (matchesPriceFormat(maxPrice)) {
                    return marketItemDao.getItemsWithMaxPrice(Double.parseDouble(maxPrice));
                }
                throw new BadRequestResponse("improper price format provided for max-price");
            }
        }

        private boolean matchesPriceFormat(String str){
            return str.matches("\\d{0,4}(\\.\\d{1,2})?");
        }

}
